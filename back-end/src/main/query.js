var createError = require('http-errors');
var express = require('express');
var path = require('path');

var queryRouter = require('./routeQuery');
var liveQueryRouter = require('./live');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/live', liveQueryRouter);
app.use('/archive', queryRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    error: {
      type: 500,
      message: "Erreur de la requête"
    }
  })
});

app.listen(80, function () {
  console.log('Example app listening on port 80!')
})