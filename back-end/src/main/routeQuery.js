//Library requirements
var template_js = require("./utils/jsonTemplates.js");
var parametersChecker = require("./utils/checkParameters.js");
const Influx = require('influx');
const csv = require('csv-parser');
const fs = require('fs');

var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
  res.header('Access-Control-Allow-Origin','*');
  let capteurs = req.query.capteurs.split(',');

  capteurs = parametersChecker.validParameters(capteurs);
  if (capteurs[0]=='invalid' || !parametersChecker.invalidDates(req.query.stop,req.query.start)){
    res.json({error: {
      type: "Invalid Paramaters"
    }
  })
  }
  else {
    let stop = new Date(req.query.stop).getTime() * 1000000;
    let start = new Date(req.query.start).getTime() * 1000000;
    const promises = [];

    let config = [];
    let jsonObj = template_js.createJson(fs);

    //Read configuration file
    fs.createReadStream('config/configuration.csv')
      .pipe(csv())
      .on('data', (row) => {
        config.push(row);
      })
      .on('end', () => {
        //CSV file successfully processed
        const influx = new Influx.InfluxDB({
          host: 'localhost',
          database: 'archiveDB',
          schema: [
            {
              measurement: 'records',
              fields: { descr: Influx.FieldType.STRING },
              tags: ['unit', 'value', 'name']
            }
          ]
        });

        let dateLatLong_query = "select time, lat, long from records"
          + " where time<" + stop + " and time>" + start
          + " order by time limit 1";

        //Get Measurements
        capteurs.forEach(function (capt) {
          promises.push(
            query_DB_capteur(capt, influx, stop, start, config).then(result => {
              var res = processMeasurements(capt, result);
              template_js.addMeasurement(fs, capt, config, jsonObj, res);
            })
          )
        })

        //Set Header and return json
        Promise.all(promises)
          .then(promises =>
            influx.query(dateLatLong_query)
              .catch(err => {
                console.log(err);
                res.json({error: {
                  type: "error"
                }
              })
              })
              .then(results => {
                updateHeader(jsonObj, results);
                res.json({result : jsonObj});
              })
          )
      });

  }
    
});

function processMeasurements(capt, result) {
  var res = [];
  if (result[0] != undefined) {
    if (capt != "win" && capt != "ran") {
      for (var i = 0; i < result.length; i++) {
        res.push([result[i]["time"]["_nanoISO"], result[i]["value"]]);
      }
    }
    else {
      if (capt == "win") {
        for (var i = 0; i < (result.length / 4); i++) {
          let datawind = [result[i]["time"]["_nanoISO"], null, null, null, null];
          for (var j = 0; j < 4; j++) {
            if (result[4 * i + j]["name"] == "wind_heading") {
              datawind[1] = parseFloat(result[4 * i + j]["value"]);
            }
            if (result[4 * i + j]["name"] == "wind_speed_avg") {
              datawind[2] = parseFloat(result[4 * i + j]["value"]);
            }
            if (result[4 * i + j]["name"] == "wind_speed_max") {
              datawind[3] = parseFloat(result[4 * i + j]["value"]);
            }
            if (result[4 * i + j]["name"] == "wind_speed_min") {
              datawind[4] = parseFloat(result[4 * i + j]["value"]);
            }
          }
          res.push(datawind);

        }
      }
      else {// Si le capteur est la pluie
        for (let i = 0; i < result.length; i++) {
          let date = [result[i]["time"]];
          res.push(date);
        }
      }
    }
  }
  return res;
}

function updateHeader(jsonObj, results) {
  if (results[0] == undefined) {
    jsonObj["coordinate"]["date"] = null;
    jsonObj["coordinate"]["latitude"] = null;
    jsonObj["coordinate"]["longitude"] = null;
    jsonObj["coordinate"]["success"] = false;

  }
  else {
    jsonObj["coordinate"]["date"] = new Date(results[0]["time"]["_nanoISO"]);
    jsonObj["coordinate"]["latitude"] = results[0]["lat"];
    jsonObj["coordinate"]["longitude"] = results[0]["long"];
  }

}

function query_DB_capteur(capteur, influx, stop, start, config) {
  query = find_query(capteur, stop, start, config);
  return influx.query(query)
    .catch(err => { console.log(err); })
    .then(results => { return (results); });
}

function find_query(capteur, stop, start, config) {
  let length_queries_list = config.length;
  for (let compt = 0; compt < length_queries_list; compt++) {
    if (config[compt]["balise"] === capteur) {
      if (config[compt]["balise"] === "ran") {
        return config[compt]["query"] + " where time<" + stop + " and time>" + start + " order by time";
      }
      else {
        let query = config[compt]["query"] + " and time<" + stop + " and time>" + start + " order by time";
        return query;
      }
    }
  }

}


module.exports = router;
