const Influx = require('influx');
var coordinateUtil = require("./utils/coordinatesUtil.js");
const fs = require('fs');
const cron = require("node-cron");
require('log-timestamp');

const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: 'archiveDB',
    schema: [
        {
            measurement: 'records',
            fields: {
                descr: Influx.FieldType.STRING,
                lat: Influx.FieldType.FLOAT,
                long: Influx.FieldType.FLOAT
            },
            tags: ['unit', 'value', 'name']
        },
        {
            measurement: 'rain',
            fields: { rain: Influx.FieldType.BOOLEAN },
            tags: ['id']
        }
    ]
});

// Function to add records to database
const writeDataToInflux = (records, date, coords) => {
    records.forEach(rec => { // For every measurement
        influx.writePoints([
            {
                measurement: 'records',
                tags: {
                    unit: rec["unit"],
                    value: rec["value"],
                    name: rec["name"],
                },
                fields: { descr: rec["desc"], lat: coords[0], long: coords[1] },
                timestamp: date,
            }
        ], {
            database: 'archiveDB'
        })
            .catch(error => {
                console.error(`Error saving data to InfluxDB! `)
            });
    });
}

const writeRainDataToDB = (date) => {
    influx.writePoints([
        {
            measurement: 'rain',
            tags: {
                id: 0
            },
            fields: { rain: true },
            timestamp: date
        }
    ], {
        database: 'archiveDB'
    })
        .catch(error => {
            console.error(error)
        });
}


// If the database doesn't exit, create it
influx.getDatabaseNames().then(
    names => {
        if (!names.includes('archiveDB')) {
            return influx.createDatabase('archiveDB');
        }
    }
);

const rainLogFile = '/dev/shm/rainCounter.log';
fs.watchFile(rainLogFile, (curr, prev) => {
    let rawdata = fs.readFileSync(rainLogFile);
    let date_rain = new Date(String.fromCharCode.apply(null, new Uint16Array(rawdata)).trim());
    console.log(date_rain);
    writeRainDataToDB(date_rain);
    console.log(`file Changed`);
});

cron.schedule("*/20 * * * *", function () {
    console.log("updating the database");
    let rawdata = fs.readFileSync('/dev/shm/sensors');
    let coordsGPS = fs.readFileSync('/dev/shm/gpsNmea');
    let coords = coordinateUtil.getDecimalDegrees(coordsGPS);
    let data = JSON.parse(rawdata);
    let date = new Date(data["date"]);
    let recordsArray = data["measure"];

    writeDataToInflux(recordsArray, date,coords);// Add to database
});



