var template_js = require("./utils/jsonTemplates.js");
var coordUtil   = require("./utils/coordinatesUtil.js");
var parametersChecker = require("./utils/checkParameters.js");
const csv = require('csv-parser');
const fs  = require('fs');

var express = require('express');
var router  = express.Router();

router.get('/', function(req, res, next) {
  res.header('Access-Control-Allow-Origin','*');
  //Parameters
  let capteurs = req.query.capteurs.split(',');
  capteurs = parametersChecker.validParameters(capteurs);
  if (capteurs[0]=='invalid'){
    res.json({error: {
      type: "Invalid Paramaters"
    }
  })
  }
  else {
  
    let config  = [];
    
    let jsonObj = template_js.createJson(fs);
    
    fs.createReadStream('config/configuration.csv')
      .pipe(csv())
      .on('data', (row) => {
        config.push(row);
      })
      .on('end', () => {
        let sensors = [];
        for (let i=0;i<capteurs.length;i++){
          let names = getFullName(capteurs[i],config).split("/");
          for (let j=0;j<names.length;j++){
            sensors.push(names[j]);
          }
        }

        if(capteurs.includes("ran")){
          let rawRain = fs.readFileSync('/dev/shm/rainCounter.log');
          let date_rain = new Date(String.fromCharCode.apply(null, new Uint16Array(rawRain)).trim());
          template_js.addMeasurement(fs,"ran",config,jsonObj,[[date_rain]]);

        }

        let rawdata = fs.readFileSync('/dev/shm/sensors');
        let data = JSON.parse(rawdata);
        let lastDate = data["date"];
        let measures = data["measure"];
        let datawind = [lastDate, null, null, null, null];
        for (let i=0; i< measures.length;i++){
          let measureName = measures[i]["name"];
          if (sensors.includes(measureName)){
            if (measureName.includes("wind")){
              if (measureName == "wind_heading") {
                datawind[1] = parseFloat(measures[i]["value"]);
              }
              if (measureName == "wind_speed_avg") {
                datawind[2] = parseFloat(measures[i]["value"]);
              }
              if (measureName == "wind_speed_max") {
                datawind[3] = parseFloat(measures[i]["value"]);
              }
              if (measureName == "wind_speed_min") {
                datawind[4] = parseFloat(measures[i]["value"]);
              }
            }
            else {
              let lastMeasures = [];
              lastMeasures.push(lastDate);
              lastMeasures.push(parseFloat(measures[i]["value"]));
              template_js.addMeasurement(fs,getNickname(measureName,config),config,jsonObj,[lastMeasures]);
            }
          }
        } 
        if(capteurs.includes("win")){
          template_js.addMeasurement(fs,"win",config,jsonObj,[datawind]);
        }

        let coordsGPS = fs.readFileSync('/dev/shm/gpsNmea');
        let coords = coordUtil.getDecimalDegrees(coordsGPS);
        updateHeader(lastDate, coords,jsonObj);
        res.json({result : jsonObj});

      })
    }
    
});

function getFullName(capteur,config){
  let length_queries_list = config.length;
  for (let compt = 0; compt < length_queries_list; compt++) {
    if (config[compt]["balise"] === capteur) {
      let name = config[compt]["full_name"];
      return name;
    }
  }
}

function getNickname(capteur,config){
  let length_queries_list = config.length;
  for (let compt = 0; compt < length_queries_list; compt++) {
    if (config[compt]["full_name"] === capteur) {
      let name = config[compt]["balise"];
      return name;
    }
  }
}

function updateHeader(lastDate, coords,jsonObj){
  jsonObj["coordinate"]["date"] = lastDate;
  jsonObj["coordinate"]["latitude"]  = coords[0];
  jsonObj["coordinate"]["longitude"] = coords[1];
}

module.exports = router;
