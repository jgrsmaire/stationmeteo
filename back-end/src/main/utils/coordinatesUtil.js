module.exports = {
    getDecimalDegrees: function(coordsGPS){
        let str = String.fromCharCode.apply(null, new Uint16Array(coordsGPS)).trim();
        let coords_str = str.split(',');
        let lat = coords_str[2];
        let long = coords_str[4];
        let coords = []; coords.push(GPGGAtoDecimalDegrees(lat)); coords.push(GPGGAtoDecimalDegrees(long));
        return coords;
    }
}

function GPGGAtoDecimalDegrees(coord){
    var integerDegree = Math.trunc(coord/100);
    var decimalDegree = coord - integerDegree*100
    var coordDec = integerDegree + decimalDegree/60
    return coordDec;
}