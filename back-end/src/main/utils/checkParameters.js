module.exports = {
    validParameters: function(capteurs){
        let correctParameter = ["hum", "tem", "ran", "pre", "win", "lum"];
        if (capteurs[0] == "all") {
            capteurs = ["hum", "tem", "ran", "pre", "win", "lum"];
        }
        for (var i=0; i< capteurs.length; i++){
            if (!correctParameter.includes(capteurs[i])){
                capteurs = ["invalid"];
                return capteurs;
            }
        }
        return capteurs;
    },
    invalidDates: function(start,stop){
        let startDate = new Date(start);
        let stopDate = new Date(start);
        if (stopDate == "Invalid Date" || startDate == "Invalid Date"){
            return false;
        }
        return true;
    }
}
