module.exports = {
    createJson: function(fs) {
        let rawHeader = fs.readFileSync("ressources/template/template_header.json");
        let header = JSON.parse(rawHeader);
        return header;},

    addMeasurement: function(fs,capteur,config,jsonObject,res){
        let rawMeasurements = fs.readFileSync("ressources/"+find_template(capteur,config));
        let measurements = JSON.parse(rawMeasurements);
        let nameCapteur = Object.keys(measurements)[0]
        measurements[nameCapteur]["data"] = res;
        jsonObject["measurements"][nameCapteur] = measurements[nameCapteur];
      }
}

function find_template(capteur,config){
  let length_queries_list = config.length;
  for (let compt = 0; compt < length_queries_list; compt++) {
    if (config[compt]["balise"] === capteur) {
      let path = config[compt]["template"];
      return path;
    }
  }
}