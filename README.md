# Projet station météo

## Objectif
L'objectif de ce projet est de réaliser la partie Software d'une station météo composée de sondes et d'une centrale. Chaque sonde est reliée à plusieurs capteurs qui donnent des informations diverses (température, hygrométrie, pression atmosphérique, pluviométrie, luminosité, vitesse et direction du vent, position GPS et heure).

### Contraintes
Utilisation exclusive des technologies WEB (HTML/CSS/JavaScript/Node.js)

## Front-end

Le code du front-end se trouve dans le dossier stationmeteo.

### Installation
Déplacez-vous dans le dossier `stationmeteo` :

```
cd stationmeteo
```

Installation des dépendances :

```
npm install
```

## Exécution du progamme

```
npm run serve
```

Le site est alors disponible à l'adresse indiquée dans l'invite de commande (http://localhost:8080 ou une autre).

## Back-end

Le code du back-end se trouve dans le dossier back-end/src/main.

**Important:** Ce projet est exécutable sur le réseau de l'ENSG, la Raspberry étant hébergée là-bas.

Le serveur est disponible à l'adresse suivante : http://piensg010.ensg.eu.

Deux routes ont été définies en accord avec les autres groupes :
* `/live` : cette route prend comme paramètres `capteurs`
* `/archive` : cette route prend comme paramètres `capteurs` ainsi que `start` et `stop` qui correspondent aux dates entre lesquelles on veut des données.

Pour plus d'informations concernant les formats des requêtes http et des décisions de groupe *cf* [notes.md](https://gitlab.com/jgrsmaire/stationmeteo/blob/master/notes.md)

### Installation

Déplacez-vous dans le dossier main du back-end :

```
cd back-end/src/main
```

Installation des dépendances :

```
npm install
```

### Démarrer le serveur

Lancez les commandes suivantes :

```
node recordsMeasurements.js
node query.js
```

### Automatisation

Allez dans le dossier contenant les fichiers `recordsMeasurements.service` et `query.service` :

```
cd /etc/systemd/system/

```

Démarrer les services :

```
sudo systemctl start recordsMeasurements.service  
sudo systemctl start query.service 
```

Pour que le service soit activé à chaque fois :

```
sudo systemctl enable recordsMeasurements.service  
sudo systemctl enable query.service 
```

### Connexion à la Raspberry

Connectez-vous à la Raspberry Pi 010 en ssh avec son adresse puis entrez le mot de passe.

### Installation de la base de données

Une fois connecté :

```
sudo apt install influxdb
sudo apt install influxdb-client
```

### Visualisation de la base de données

```
influx
use archiveDB
select * from records
```

## Authors
* Geniet Florent
* Grosmaire Julie
* Stretti Marie