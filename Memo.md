## Choix des technologies :
Vue
Serveur : Node.js
"BDD" : InfluxDB (sur Docker si possible)

## Tâches :
### Front-end :
- Organisation générale du site : Trois -> Generale (Carto), live par defaut + Une page pour info capteur par type (archive (input avec les dates, dates par default à l'ouverture de la page) graphique, live) + choix des capteurs pour chaque type )
- Carto des sondes (positionnement dans la trame GPS), query sur les informations meteo 
- Affichage des données (graphique + instantané), avec sonde qui correspond (sonde en surbrillance?)
- CSS : responsive
- Tests unitaires (vue cli)

### Back-end :
- x Enregistrement des données dans la BDD (en node.js de préférence) + supression sauf la dernière donnée écrite : 
- (manque erreur) API mise à disposition de la dernière valeur d'une donnée d'un capteur (récupération en mémoire locale, utilisation cache ?) avec mise en forme correcte en json
- x API mise à disposition d'un échantillon de données (récupération dans la base de données) avec mise ne forme correcte en json
- x Création de paire de clef rsa pour accès
- Tests Unitaires (écriture dans la base de données et récupération (mock))
- x Convertir en degré les coordonnées !!

## Info RaspberryPi :
piensg(009->011,018) / pi/raspberry



## Idées noms
- WhetherIGN